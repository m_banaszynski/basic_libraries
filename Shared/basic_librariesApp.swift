//
//  basic_librariesApp.swift
//  Shared
//
//  Created by Maciej Banaszyński on 06/07/2021.
//

import SwiftUI

@main
struct basic_librariesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
