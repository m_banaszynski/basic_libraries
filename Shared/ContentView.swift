//
//  ContentView.swift
//  Shared
//
//  Created by Maciej Banaszyński on 06/07/2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, app!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
